from pymongo import MongoClient

import irc.bot
import irc.strings

from contextlib import contextmanager
import inspect
import traceback

class Seabird(irc.bot.SingleServerIRCBot):
    def __init__(self, config):
        irc.bot.SingleServerIRCBot.__init__(
                self,
                config['servers'],
                config['nicknames'][0],
                config['realname'] or config['nickname'],
        )

        self.config = config
        self.plugins = {}

        self.db_client = MongoClient()
        # TODO: Make db name configurable
        self.db = self.db_client.seabird

    def plugin(self, obj):
        if inspect.isclass(obj):
            o = obj()
            o.bot = self
            o.config = self.config
            
            self.plugins[type(o).__name__] = o
        else:
            raise Exception('Plugin must be a class')

        return obj

    def dispatch(self, method, *args, **kwargs):
        print(method)
        for obj in self.plugins.values():
            f = getattr(obj, method, None)
            if callable(f):
                print(obj)
                # TODO: wrap_err this if it's not already wrapped
                f(*args, **kwargs)

    def on_nicknameinuse(self, c, e):
        cur = c.get_nickname()
        try:
            i = self.config['nicknames'].index(cur)
            c.nick(self.config['nicknames'][i+1])
        except:
            c.nick(cur+"_")

    def on_welcome(self, c, e):
        if 'channels' in self.config['startup']:
            for channel in self.config['startup']['channels']:
                c.join(channel)
        with self.wrap_err():
            self.dispatch("on_welcome", c, e)

    @contextmanager
    def wrap_err(self, loc=None, prefix=None):
        try:
            yield
        except Exception as e:
            traceback.print_exc()
            if loc:
                if prefix:
                    self.connection.privmsg(loc, '%s: %s' % (prefix, e))
                else:
                    self.connection.privmsg(loc, '%s' % e)

    def on_privmsg(self, c, e):
        with self.wrap_err(e.source.nick):
            self.dispatch("on_privmsg", c, e)

    def on_pubmsg(self, c, e):
        channel = e.target
        user = e.source.nick
        msg = e.arguments[0]

        with self.wrap_err(channel, user):

            print('%s <%s> %s' % (channel, user, msg))

            a = msg.split(":", 1)
            if len(a) > 1 and irc.strings.lower(a[0]) == irc.strings.lower(c.get_nickname()):
                self.dispatch("on_mention", e, a[1].strip())
            elif msg.startswith(self.config['prefix']):
                # TODO: Make this readable and less error prone
                a = msg[len(self.config['prefix']):].split(" ", 1)
                self.dispatch("on_command", c, e, a[0], msg[len(a[0])+1:].strip())
            else:
                self.dispatch("on_pubmsg", c, e)
