from .. import bot

import random

responses = {
    'botsnack':     [':)', ';x'],
    'bot snack':    [':)', ';x'],
    'scoobysnack':  ['Scooby Dooby Doo!'],
    'scooby snack': ['Scooby Dooby Doo!'],
    'hi':           ['hello'],
    'hello':        ['hello'],
    'fuck you':     ['well, fuck you too!'],
    'thank you':    ['you\'re welcome!'],
    'thank-you':    ['you\'re welcome!'],
    'thanks':       ['you\'re welcome!'],
}

@bot.plugin
class Mentions(object):
    def on_mention(self, e, msg):
        c = self.bot.connection
        channel = e.target
        user = e.source.nick
        msg = msg.lower()

        if msg in responses:
            r = responses[msg]
            r = r[random.randint(0, len(r)-1)]
            c.privmsg(channel, '%s: %s' % (user, r))
