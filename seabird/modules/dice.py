from .. import bot

import random
import re

dice_re = re.compile('(?:^|\\b)(\d*)d(\d+)\\b')

@bot.plugin
class Dice(object):
    def on_pubmsg(self, c, e):
        channel = e.target
        user = e.source.nick
        msg = e.arguments[0]

        total_count = 0
        out = []

        matches = dice_re.findall(msg)
        for (count, size) in matches:
            if not count:
                count = 1

            count = int(count)
            size = int(size)

            total_count += count

            if total_count > 100:
                raise Exception('You cannot request more than 100 rolls')

            if size > 100:
                raise Exception('You cannot request dice larger than 100')

            vals = []
            for i in xrange(0, count):
                vals.append(random.randint(1, size))

            if vals:
                msg = ', '.join(str(x) for x in vals)
            else:
                msg = 'Error'

            out.append('%dd%d: %s' % (count, size, msg))

        if out:
            data = ' '.join(out)
            c.privmsg(channel, '%s: %s' % (user, data))

