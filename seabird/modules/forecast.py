from .. import bot

import forecastio
import requests

def geocode(location):
    r = requests.get(
        "http://maps.googleapis.com/maps/api/geocode/json",
        params={'address': location, 'sensor': 'false'}, timeout=0.5)

    data = r.json()
    if len(data['results']) == 1:
        r = data['results'][0]
        g = r['geometry']['location']
        return r['formatted_address'], (g['lat'], g['lng'])

    raise Exception('Not a valid location')

@bot.plugin
class ForecastIO(object):
    def on_command(self, c, e, cmd, msg):
        c = self.bot.connection

        channel = e.target
        user = e.source.nick

        if cmd == 'fforecast':
            addr, (lat, lon) = geocode(msg)
            forecast = forecastio.load_forecast(self.config['forecastio_api_key'], lat, lon)
            block = forecast.daily()
            out = '%s: 3 day forecast for %s.' % (user, addr)
            c.privmsg(channel, out)
            for point in block.data[0:3]:
                out = '%s: High %.2f, Low %.2f, %s' % (user, point.temperatureMax, point.temperatureMin, point.summary)
                c.privmsg(channel, out)
        elif cmd == 'fweather':
            addr, (lat, lon) = geocode(msg)
            forecast = forecastio.load_forecast(self.config['forecastio_api_key'], lat, lon)
            point = forecast.currently()
            out = '%s: %s. Currently %.1f. %s.' % (user, addr, point.temperature, point.summary)
            c.privmsg(channel, out)
