from .. import bot

from pymongo import MongoClient

import re

karma_re = re.compile('((?:\w+[\+-]?)*\w)(\+\+|--)(?:\s|$)')

@bot.plugin
class Karma(object):
    def on_pubmsg(self, c, e):
        channel = e.target
        user = e.source.nick
        msg = e.arguments[0]

        matches = karma_re.findall(msg)
        for (item, op) in matches:
            if op == '++':
                diff = 1
            else:
                diff = -1
            self.bot.db.karma.update({'name': item}, {'$inc': {'score': diff}}, upsert=True)
            ret = self.bot.db.karma.find_one({'name': item})
            c.privmsg(channel, '%s: %s\'s karma is now %d' % (user, item, ret['score']))
