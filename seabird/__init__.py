from .seabird import Seabird

config = {
    'nicknames': ['seabird'],
    'realname': 'Seabird',
    'username': 'seabird',
    'prefix': '!',

    'servers': [
        ('chat.freenode.net', 6667, 'password'),
    ],

    'forecastio_api_key': 'key_here',

    'startup': {
        'channels': ['#test-chan'],
    }
}

bot = Seabird(config)
